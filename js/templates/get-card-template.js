export const getCardTemplate = ({id, name, thumbnail, description}) => (
    `<div class="card">
        <p>${name}</p>
        <img src="${thumbnail}" alt="${name}" class="card-image">
        <p>${description}</p>
        <a href="/${id}">${name}</a>
    </div>`
);
